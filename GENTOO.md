## Gentoo build

Try to make step-by-step manual to build Gentoo on X1

```bash
parted -a optimal /dev/nvme0n1
# create Grub bios partition
-> mklabel gpt 
-> print
-> unit mib
-> mkpart primary 1 3
-> name 1 grub
-> set 1 bios_grub on
-> print
# create Boot partition
-> mkpart primary 3 515
-> name 2 boot
-> set 2 boot on
# create LVM partition
-> mkpart primary 515 -1
-> name 3 lvm_luks
-> quit
# create FAT32 FS for boot partition
mkfs.fat -F 32 /dev/nvme0n1p2

# encrypt future LVM patition
modprobe dm-crypt
cryptsetup luksFormat -c aes-xts-plain64:sha256 -s 512 /dev/nvme0n1p3

# create LVM inside encrypted partition
cryptsetup luksOpen /dev/nvme0n1p3 lvm
lvm pvcreate /dev/mapper/lvm
vgcreate xcarbon-vg /dev/mapper/lvm
lvcreate -l 100%FREE -n root xcarbon-vg
lvreduce -L -32G /dev/xcarbon-vg/root
lvcreate -l 100%FREE -n swap xcarbon-vg

# create filesystems
mkfs.ext4 /dev/xcarbon-vg/root
mkswap /dev/xcarbon-vg/root

swapon /dev/xcarbon-vg/swap
mount /dev/xcarbon-vg/root /mnt/gentoo

# unpack stage3 
cp /livemnt/backstore/stage3/stage3-amd64-20180906T214502Z.tar.xz /mnt/gentoo/
cd /mnt/gentoo/
tar xpf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
rm stage3*.xz

# -----
# Copy files in /etc here
# -----

# Chroot magic
/root/gentoo-x1/mx1.sh
chroot /mnt/gentoo bash
source /etc/profile

# Sync portage
emerge-webrsync
emerge --sync
emerge layman

# select profile
layman -a unity-gentoo
eselect profile list 
eselect profile set 55

# rebuild base compiler stuff
emerge glibc
emerge gcc
emerge dev-util/ccache
CCACHE_DIR="/var/tmp/ccache" ccache -s
emerge --update --deep --newuse @system
emerge --update --deep --newuse @world
emerge -e @world

# configure timezone and locales
emerge --config sys-libs/timezone-data
locale-gen
eselect locale list
eselect locale set 4
source /etc/profile 

# pull system sources
emerge --ask sys-kernel/gentoo-sources

# configure grub and fstab
blkid >> /etc/fstab
blkid >> /etc/default/grub
# ... and vim them
emerge vim app-misc/mc htop
emerge --ask sys-apps/pciutils

# configure efi
emerge --ask efibootmgr
mount | grep efivars
mount -o rw,remount /sys/firmware/efi/efivars
# delete old entries
efibootmgr -b 2 -B

# compile kernel
emerge --ask sys-kernel/genkernel-next bzip gzip sys-kernel/linux-firmware dhcpcd iw wpa_supplicant lzop
genkernel --menuconfig --kernel-config=/root/kernels/kernel.conf

# emerge nesessary stuff
emerge cronie tmux mlocate e2fsprogs xfsprogs reiserfsprogs jfsutils dosfstools btrfs-progs eix emerge net-misc/networkmanager sudo nano lshw acpid apmd bluez laptop-mode-tools pm-utils virtualenv libdbus dev-python/pip pycairo git gentoolkit 

# emerge grub
echo "sys-boot/grub:2 device-mapper" >> /etc/portage/package.use/grub
emerge sys-boot/grub:2 app-misc/pax-utils

# enable lvm autostart
rc-update add lvm boot 
systemctl enable lvm2-monitor

# install grub2
grub-install --target=x86_64-efi --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg

# unemerge udev - it is provided by systemd
emerge -C sys-fs/udev
emerge --deselect sys-fs/udev

# enable dbus
systemctl status dbus
systemctl enable dbus

# enable dhcpcd
systemctl enable dhcpcd
systemctl start dhcpcd

# emerge unity desktop
emerge -av unity-build-env
emerge -uDNavt --backtrack=30 unity-meta

# fix line endings to unix if emerge fail with perl not found
vim /usr/lib/gtk-sharp-2.0/gapi2xml.pl 
vim /usr/lib/gtk-sharp-2.0/gapi_pp.pl 

# enable lightdm
systemctl enable lightdm
systemctl start lightdm

# enable sudo group
vim /etc/sudoers

# add working user to essential groups
usermod -aG adm,plugdev,lpadmin,sudo,cdrom,floppy,usb,input user

# change virtual console font
vim /etc/vconsole.conf 
systemctl restart systemd-vconsole-setup.service

# change hostname, locale, timezone
hostnamectl set-hostname carbon
localectl set-locale LANG="ru_RU.utf8"
timedatectl set-timezone "Europe/Moscow"

# setup cron for systemd
emerge systemd-cron
systemctl enable cron.target
systemctl start cron.target

# enable some installed services 
systemctl enable bluetooth
systemctl enable acpid
systemctl enable laptop-mode

# fix synaptics touchpad issues
vim /etc/modprobe.d/psmouse.conf 
vim /etc/systemd/system/wakeup-touchpad.service
systemctl enable --now wakeup-touchpad

# lenovo throttling fix
git clone https://github.com/erpalma/lenovo-throttling-fix.git
./lenovo-throttling-fix/install.sh 
vim /etc/lenovo_fix.conf 
systemctl status lenovo_fix

# cardreader fix 
vim /etc/systemd/system/wakeup-cardreader.service 
systemctl enable wakeup-cardreader

# emerge some desktop software
emerge google-chrome nm-applet unity-lens-meta gnome-core-apps gconf-editor gnome-tweak-tool xf86-input-synaptics guake nvme-cli net-im/telegram-desktop-bin remmina

# enable touchpad gestures
emerge x11-misc/libinput-gestures
libinput-gestures-setup autostart
vim ~/.config/libinput-gestures.conf

# load nesessary modules
vim /etc/modules-load.d/x1-carbon-6th.conf

# some virt stuff
layman -a stefantalpalaru
emerge virtualbox docker lxd lxcfs emerge app-emulation/vmware-workstation app-emulation/vagrant
systemctl enable lxd
systemctl enable docker
vim /etc/systemd/system/vmware-networks.service
vim /etc/systemd/system/vmware-usbarbitrator.service 
cat /etc/modules-load.d/vmware.conf
systemctl enable vmware-networks
systemctl enable vmware-usbarbitrator
usermod -aG vboxusers,docker,lxd,vmware user

# kerio vpn
wget http://cdn.kerio.com/dwn/control/control-9.2.7-2921/kerio-control-vpnclient-9.2.7-2921-linux-amd64.deb
ar x kerio-control-vpnclient-9.2.7-2921-linux-amd64.deb
# unpack files manualy here - be carefull
vim /lib/systemd/system/kerio-kvc.service 
vim /etc/kerio-kvc.conf 
systemctl enable kerio-kvc
# sudo ./config 
# sudo ./postinst 


# gesettings
while read line; do echo gsettings set $line; gsettings set $line; done<<< `cat gsettings.txt | sort`
gsettings set org.gnome.desktop.default-applications.terminal exec 'gnome-terminal'
gsettings set com.canonical.Unity.Lenses always-search "['applications.scope', 'calendar.scope', 'files.scope', 'commands.scope', 'info.scope', 'home.scope']"

```