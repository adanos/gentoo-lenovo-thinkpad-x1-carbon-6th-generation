#!/bin/bash
# -----
# Script to umount Gentoo on x1 from LiveUSB
# -----

umount -l /mnt/gentoo/dev{/shm,/pts,}
umount -R /mnt/gentoo
