#!/bin/bash
# -----
# Script to mount Gentoo on x1 from LiveUSB
# -----

cryptsetup luksOpen /dev/nvme0n1p3 x1
sleep 2
mount /dev/xcarbon-vg/root /mnt/gentoo
swapon /dev/xcarbon-vg/swap
cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
